<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_AI Sound</name>
   <tag></tag>
   <elementGuidId>bf0df215-03eb-483e-963c-c3dcfb41a1ca</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div[title=&quot;AI Sound&quot;]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='__next']/div/div/div/div/div/div/ul/li[3]/div[2]/div/ul/li[2]/a/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>d13b9c6a-472c-42d7-b93e-e5c17b651692</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>AI Sound</value>
      <webElementGuid>0a5a4908-a4ec-4e29-99ae-a793ee1a66e1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>AI Sound</value>
      <webElementGuid>bebe0e2d-1530-4d4e-9619-f44603a19d56</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;__next&quot;)/div[1]/div[@class=&quot;style_headerWrapper__ffo9A style_bgLight__poLOJ&quot;]/div[1]/div[@class=&quot;container&quot;]/div[@class=&quot;style_navbarWrapper__cL__c&quot;]/div[@class=&quot;style_navbarActionWrapper__nO10b&quot;]/ul[@class=&quot;style_navbarMenuList__rF6Ee&quot;]/li[@class=&quot;style_menuDropdown__byKZ_&quot;]/div[@class=&quot;style_dropdown__RN0IT&quot;]/div[1]/ul[1]/li[2]/a[@class=&quot;style_childMenu__9WlE6&quot;]/div[1]</value>
      <webElementGuid>162751bb-94cd-4a4b-b1e1-1edcf5ca6498</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='__next']/div/div/div/div/div/div/ul/li[3]/div[2]/div/ul/li[2]/a/div</value>
      <webElementGuid>f8a46900-2838-454a-ac28-1755cabaca56</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/ul/li[2]/a/div</value>
      <webElementGuid>abc8df0b-820c-44c8-9496-1a020614da6d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@title = 'AI Sound' and (text() = 'AI Sound' or . = 'AI Sound')]</value>
      <webElementGuid>7973f781-63f7-4a7c-be10-09fa708311d2</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
